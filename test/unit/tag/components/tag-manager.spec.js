// eslint-disable-next-line
import { TagManager } from 'tag/components/tag-manager';
import TestingPromiseHelper from '../../helpers/testing-promise-helper';

describe('TagManager', () => {
  let tagManager;
  let promiseHelper;
  let mockEventAggregator;

  function initializeWithMocks() {
    const mockTagService = {
      setViewModel: () => {},
      deleteTag: () => promiseHelper.promise,
    };

    const mockUserTagService = { setViewModel: () => {} };
    mockEventAggregator = { publish: () => {} };
    const mockApplicationParameter = { getParameters: () => {} };

    spyOn(mockEventAggregator, 'publish');

    tagManager =
      new TagManager(mockTagService, mockUserTagService, mockEventAggregator,
         mockApplicationParameter);
  }

  beforeEach(() => {
    promiseHelper = new TestingPromiseHelper();

    initializeWithMocks();
  });

  describe('#deleteTag', () => {
    beforeEach(() => {
      tagManager.tagSelected = { id: 3 };
    });

    it('should notify the event of delete-tag', (done) => {
      tagManager.deleteTag()
        .then(() => {
          expect(mockEventAggregator.publish)
            .toHaveBeenCalledWith('tag-deleted', { id: 3 });
          done();
        });
      promiseHelper.decider.resolve();
    });
  });
});
