import { Tag } from '../../../src/models/tag';

describe('Tag', () => {
  let someTag;

  describe('ctor', () => {
    it('should copy id and name sent', () => {
      someTag = new Tag({ id: 3, name: 'radicals' });
      expect(someTag.id).toBe(3);
      expect(someTag.name).toBe('radicals');
    });

    it('should use default value for name when only id is sent', () => {
      someTag = new Tag({ id: 5 });
      expect(someTag.id).toBe(5);
      expect(someTag.name).toBe('');
    });

    it('should use default value for name when nothing is sent', () => {
      someTag = new Tag();
      expect(someTag.name).toBe('');
    });
  });

  describe('isValid', () => {
    it('should return true if the tag has valid chars and length <= 30', () => {
      someTag = new Tag({ name: 'ruby masters' });
      expect(someTag.isValid).toBe(true);
    });

    it('should return false if tag is created with default values', () => {
      someTag = new Tag();
      expect(someTag.isValid).toBe(false);
    });
  });

  describe('isDuplicatedIn', () => {
    const tagList = [{ name: 'ruby masters' }, { name: 'clean coders' }, { name: 'javers' }];

    it('should return true if tag list contains it', () => {
      someTag = new Tag({ name: 'javers' });
      expect(someTag.isDuplicatedIn(tagList)).toBe(true);
    });

    it('should return false if tag list does not contain it', () => {
      someTag = new Tag({ name: 'python monthy lovers' });
      expect(someTag.isDuplicatedIn(tagList)).toBe(false);
    });
  });
});
