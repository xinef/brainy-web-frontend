import { inject } from 'aurelia-framework';
import { BaseComponent } from '../../util/base-component';
import { ProfileService } from '../../profile/services/profile-service';
import { AuthHelper } from '../../util/auth-helper';
import { constants } from '../../util/constants';
import localStorageManager from '../../util/local-storage-manager';
import ApplicationParameter from '../../shared/application-parameter';
import ManagementService from '../../management/services/management-service';
import ParametersUpdate from '../../util/parameters-update';
import Parameter from '../../models/parameter';


@inject(ProfileService, AuthHelper, ApplicationParameter, ManagementService, ParametersUpdate)
export class Landing extends BaseComponent {

  constructor(profileService, authHelper, applicationParameter, managementService,
    parametersUpdate) {
    super();
    this.profileService = profileService;
    this.authHelper = authHelper;
    this.applicationParameter = applicationParameter;
    this.managementService = managementService;
    this.parametersUpdate = parametersUpdate;
    this.profileService.setViewModel(this);
    this.firstLogin = false;
  }

  login() {
    this.authHelper.login();
  }

  activate(params) {
    this.params = this.authHelper.parseQueryString(params.token);
  }

  attached() {
    this.validateUrlParams();
    this.notify('enterLandingPage');
  }

  detached() {
    this.notify('leaveLandingPage');
  }
  validateUrlParams() {
    const accessToken = this.params[constants.ACCESS_TOKEN];

    if (this.authHelper.validateJWT(accessToken)) {
      localStorageManager.setAuthToken(accessToken);
      this.loadProfile();
    } else if (accessToken) {
      this.showMessageError('Please use the corporate e-mail account.');
    } else if (this.auth.isAuthenticated()) {
      this.navigation.navigateToHome();
    }
  }

  loadProfile() {
    this.profileService.loadInitialProfileInformation()
      .then((response) => {
        if (response.status === constants.SUCCESS_STATUS) {
          response.json()
            .then(this.loadProfileSuccess.bind(this))
            .then(this.storeParametersAndRedirect.bind(this));
        } else {
          localStorageManager.removeAuthToken();
          this.navigation.navigateToLogin();
        }
      });
  }

  loadProfileSuccess(user) {
    this.firstLogin = user.firstLogin;
    if (user.headquarter !== null) {
      const isAdmin = this.applicationParameter.computeIsAdmin();
      this.applicationParameter.setCurrentHeadquarter(user.headquarter);
      localStorageManager.setHasDashboard(user.position !== '' && user.position !== '-');
      return this.parametersUpdate.getParamsAndHqs(user, isAdmin);
    }
    localStorageManager.setHasDashboard(false);
    return new Promise(resolve => resolve(Object.assign([new Parameter(), []])));
  }

  storeParametersAndRedirect(values) {
    const isAdmin = this.applicationParameter.computeIsAdmin();
    this.applicationParameter.processValues(values);
    localStorageManager.setIsAdmin(isAdmin);
    if (this.firstLogin) {
      localStorageManager.setFirstLogin(true);
      this.navigation.navigateToProfile();
    } else {
      this.navigation.navigateToHome();
    }
  }
}
