import { bindable, inject, LogManager } from 'aurelia-framework';
import { SkillBarService } from '../../services/skill-bar-service';
import { BaseComponent } from '../../../util/base-component';

@inject(SkillBarService)
export class SkillRating extends BaseComponent {

  @bindable() user;
  @bindable() updateProperty;

  constructor(api) {
    super();
    this.api = api;
    this.logger = LogManager.getLogger(SkillRating.name);
  }

  endorse(skill) {
    if (!this.user.isCurrentUser()) {
      this.processUpdateEndorse(skill);
    }
  }

  updateRateEndorse() {
    return (skill) => {
      this.endorse(skill);
    };
  }

  processUpdateEndorse(skill) {
    this.api.makeSkillEndorsement(this.user.email, skill)
      .then((response) => {
        skill.setAverageLevel(response.averageLevel);
        skill.setValidated(true);
      })
      .catch((error) => {
        this.logger.error('Details of the error:', error);
        this.showMessageError();
      });
  }

  deleteRateEndorse(skill) {
    if (!this.user.isCurrentUser()) {
      this.processDeleteEndorse(skill);
    }
  }

  processDeleteEndorse(skill) {
    this.api.deleteSkillEndorsement(this.user.email, skill)
      .then((response) => {
        skill.setAverageLevel(response.averageLevel);
        skill.setGuestLevel(null);
        skill.setValidated(false);
      })
      .catch((error) => {
        this.logger.error('Details of the error:', error);
        this.showMessageError();
      });
  }

}
