import { bindable, inject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { ProfileService } from '../../services/profile-service';
import { UserTagService } from '../../../tag/services/user-tag-service';
import { BadgeService } from '../../services/badge-service';
import { AcknowledgmentService } from '../../services/acknowledgment-service';
import { BaseComponent } from '../../../util/base-component';
import localStorageManager from '../../../util/local-storage-manager';

@inject(ProfileService, UserTagService, BadgeService, AcknowledgmentService,
   BaseComponent, EventAggregator)
export class ProfileView {

  @bindable() badges = [];
  @bindable() usertags = [];

  constructor(profileService, userTagService, badgeService,
    acknowledgmentService, baseComponent, eventAggregator) {
    this.profileService = profileService;
    this.userTagService = userTagService;
    this.badgeService = badgeService;
    this.acknowledgmentService = acknowledgmentService;
    this.baseComponent = baseComponent;
    this.profileService.setViewModel(this);
    this.badgeService.setViewModel(this);
    this.userTagService.setViewModel(this);
    this.acknowledgmentService.setViewModel(this);
    this.eventAggregator = eventAggregator;
  }

  activate(paramsFromUrl) {
    this.username = paramsFromUrl.username;
  }

  attached() {
    this.profileService
      .retrieveProfileInformation(this.username)
      .then(() => {
        this.badgeService.retrieveBadges(this.username);
        this.userTagService.getMyTagsOnUser(this.username);
        this.acknowledgmentService.retrieveAwards(this.user.email, this.user.headquarter);
      });
  }

  tagUser(selectedTag) {
    this.userTagService.tagUser(this.username, selectedTag);
  }

  deleteTagOnUser(idTag) {
    this.userTagService.deleteTagOnUser(this.username, idTag);
  }

  setTagsOnUser(tagsOnUser) {
    this.usertags = tagsOnUser;
  }

  setUser(user) {
    this.user = user;
  }

  get isAwarded() {
    return this.awards && this.awards.length > 0;
  }

  get isSamePersonToNominate() {
    const jsonBody = localStorageManager.getJsonWebTokenBody();
    return this.user && this.user.email === jsonBody.unique_name;
  }

  get currentUserHasDashboard() {
    return localStorageManager.hasDashboard();
  }

  setAwards(awards) {
    this.awards = awards;
  }

  setBadges(badges) {
    this.badges = badges;
  }

  back() {
    if (this.user.isCurrentUser()) {
      this.baseComponent.navigation.navigateToProfile();
    } else {
      this.baseComponent.navigation.navigateToHome();
    }
  }

  openNominateModal(user) {
    this.eventAggregator.publish('nominate-modal', user);
  }
}
