import { inject, CompositionTransaction } from 'aurelia-framework';
import { ProfileService } from '../../services/profile-service';
import { BadgeService } from '../../services/badge-service';
import { Navigation } from '../../../util/navigation';
import localStorageManager from '../../../util/local-storage-manager';
import ApplicationParameter from '../../../shared/application-parameter';
import ManagementService from '../../../management/services/management-service';
import { BaseComponent } from '../../../util/base-component';
import constantsMessage from '../../../util/constants-message';

@inject(ProfileService, BadgeService, Navigation, ApplicationParameter
  , ManagementService, CompositionTransaction, BaseComponent)
export class ProfileUpdate {

  isFirstLogin = false;

  constructor(profileService, badgeService, navigation, applicationParameter,
    managementService, compositionTransaction, baseComponent) {
    this.navigation = navigation;
    this.profileService = profileService;
    this.badgeService = badgeService;
    this.profileService.setViewModel(this);
    this.badgeService.setViewModel(this);
    this.managementService = managementService;
    this.applicationParameter = applicationParameter;
    this.compositionTransactionNotifier = compositionTransaction.enlist();
    this.baseComponent = baseComponent;
    this.locations = {};
    this.user = {};
    this.headquarterName = '';
    this.headquarters = [];
    this.hasHeadquarter = true;
  }

  get FirstLogin() {
    return this.isFirstLogin;
  }

  setUser(value) {
    this.user = value;
    this.hasHeadquarter = this.user.headquarter !== '';
  }

  getBadgesLength() {
    return this.badges.length;
  }

  setBadges(badges) {
    this.badges = badges;
  }

  created() {
    this.baseComponent.showProgressHub();
    this.isFirstLogin = localStorageManager.isFirstLogin();
    const email = localStorageManager.getEmailUser(); // from TOKEN
    const p1 = this.getUserProfile(email);
    const p2 = this.getUserBadges(email);
    const p3 = this.applicationParameter.getParameters();
    Promise.all([p1, p2, p3]).then(this.informationLoaded.bind(this));
  }

  attached() {
    this.profileService.notifyUpdateNavBarWhenUserLogged();
  }

  informationLoaded(values) {
    this.setParameters(values[2]);
    this.compositionTransactionNotifier.done();
    this.baseComponent.dismissProgressHub();
  }

  setParameters(model) {
    this.locations = model.locations();
    this.headquarters = model.headquarters;
    model.headquarters.forEach((headquarter) => {
      if (headquarter.code === this.user.headquarter) {
        this.headquarterName = headquarter.name;
      }
    });
  }

  getUserProfile(email) {
    return this.profileService.retrieveProfileInformation(email);
  }

  headquarterChanged(event) {
    this.getParametersByHeadquarter(event.srcElement.value);
  }

  getParametersByHeadquarter(headquarter) {
    this.managementService.getParameters(headquarter).then((parameters) => {
      this.locations = parameters.parameterProperty.location;
    })
    .then(() => {
      this.profileBasicViewModel.selectLocation.refresh();
    });
  }


  getUserBadges(email) {
    if (!this.isFirstLogin) {
      return this.badgeService.retrieveBadges(email);
    }
    return null;
  }

  save() {
    if (this.isValidProfileInformation()) {
      this.profileService.saveUserInformation(this.user)
        .then((success) => {
          if (success) {
            this.saveSuccess();
          }
        });
    } else {
      this.baseComponent.showMessageError(constantsMessage.PROFILE_INVALID_INFORMATION);
    }
  }

  saveSuccess() {
    this.isFirstLogin = false;
    this.badgeService.checkNewBadges(this.user.email);
  }

  backHome() {
    this.navigation.navigateToHome();
  }

  navigateToProfileOfMe() {
    this.navigation.navigateToProfileOf(this.user.email);
  }

  isValidProfileInformation() {
    return this.user.location !== '' && this.user.skype && this.user.skype !== '';
  }
}
