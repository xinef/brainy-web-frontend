import { EventAggregator } from 'aurelia-event-aggregator';
import { inject } from 'aurelia-framework';
import { AcknowledgmentService } from '../services/acknowledgment-service';
import { constants } from '../../util/constants';
import { constantsNominations } from '../../util/constant-nomination-messages';
import nominationUtil from '../util/nomination-util';
import HtmlToExcelExport from '../../util/html-to-excel-export';
import datePickerConfiguration from '../../util/date-picker-configuration';
import ApplicationParameter from '../../shared/application-parameter';
import { BaseComponent } from '../../util/base-component';

@inject(AcknowledgmentService, HtmlToExcelExport, ApplicationParameter,
  BaseComponent, EventAggregator)
export default class AcknowledgmentManager {

  constructor(acknowledgmentService, htmlToExcelExport, applicationParameter,
    baseComponent, eventAggregator) {
    this.acknowledgmentService = acknowledgmentService;
    this.eventAggregator = eventAggregator;
    this.htmlToExcelExport = htmlToExcelExport;
    this.applicationParameter = applicationParameter;
    this.baseComponent = baseComponent;
    this.nominations = [];
    this.isExportVisible = false;
    this.groupedNominations = [];
    this.recognizedNominations = [];
    this.searchNomination = {
      nominatorName: '',
      nomineeName: '',
      competence: '',
      status: '',
      startDate: '',
      endDate: '',
      validationCodes: [],
      page: constants.DEFAULT_LAST_PAGE,
      items: constants.ITEMS_PAGINATION };
    this.parametersPromise = applicationParameter.getParameters();
    this.parametersPromise.then(this.setParameters.bind(this));
    this.competencies = {};
    this.statuses = constants.NOMINATION_STATUSES;
    this.nominationMessages = constantsNominations.NOMINATION_MESSAGECODE;
    this.selectedNomination = {};
    this.dataPickerOptions = datePickerConfiguration.advancedOptions;
    this.searchNomination.isValid = this.validation;
    this.searchNomination.isValidName = this.isValidName;
    this.loading = false;
  }

  validation() {
    const self = this;
    const errors = [];
    if ((!!self.startDate && !!self.endDate)
        || self.nomineeName !== constants.EMPTY_STRING
        || self.nominatorName !== constants.EMPTY_STRING) {
      if (self.startDate > self.endDate) {
        errors[errors.length] = 'validation.range.invalid';
      }
      if (self.isValidName(self.nomineeName)) {
        errors[errors.length] = 'validation.nomineeName.invalid';
      }
      if (self.isValidName(self.nominatorName)) {
        errors[errors.length] = 'validation.nominatorName.invalid';
      }
    }

    self.validationCodes = errors;
    return errors.length < 1;
  }

  isValidName(n) {
    return !constants.NAME_REGEX.test(n) && n !== constants.EMPTY_STRING;
  }

  bind() {
    this.subscribeEvents();
  }

  subscribeEvents() {
    const that = this;
    this.subscriptionChangeHeadquarter = this.eventAggregator
    .subscribe('application-parameter-change-headquarter',
    (result) => {
      this.competencies = result.model.parameterProperty.competence;
      this.searchNomination.headquarter = result.model.code;
      that.search();
    });
  }

  unbind() {
    this.subscriptionChangeHeadquarter.dispose();
  }

  setParameters() {
    const current = this.applicationParameter.getCurrentParameters();
    this.searchNomination.headquarter = current.code;
    this.competencies = current.parameterProperty.competence;
  }

  getNominations() {
    this.searchNomination.page = constants.DEFAULT_LAST_PAGE;
    this.acknowledgmentService.getNominations(this.searchNomination)
      .then(this._processSuccess.bind(this))
      .catch(this._processError.bind(this));
  }

  moreNominations() {
    this.loading = true;
    this.searchNomination.page = parseInt(this.nominations.length / constants.ITEMS_PAGINATION,
      constants.DEFAULT_RADIX);
    this.acknowledgmentService.getNominations(this.searchNomination)
      .then(this._addNominations.bind(this))
      .catch(this._processError.bind(this));
  }

  _processSuccess(response) {
    this.total = 0;
    if (response.length !== 0) {
      this.total = parseInt(response[0].id, constants.DEFAULT_RADIX);
      response.shift();
    }
    this.nominations = response;
    this.isExportVisible = (this.nominations.length !== 0);
    this.groupedNominations = nominationUtil.groupNominations(this.nominations);
    this.stillMoreRows = this.nominations.length < this.total;
  }

  _addNominations(response) {
    this.nominations = this.nominations.concat(response);
    this.groupedNominations = nominationUtil.groupNominations(this.nominations);
    this.stillMoreRows = this.nominations.length < this.total;
    this.loading = false;
  }

  _processError(error) {
    if (error.messageCode) {
      const err = this.nominationMessages[error.messageCode];
      this.baseComponent.showMessageError(err);
    }
  }

  search() {
    if (!this.searchNomination.isValid()) {
      this.searchNomination.validationCodes.forEach((element) => {
        this.baseComponent.showMessageError(this.nominationMessages[element]);
      }, this);

      return;
    }
    this.parametersPromise.then(this.getNominations.bind(this));
  }

  reviewNomination(nomination) {
    this.selectedNomination = nomination;
    this.reviewNominationModal.modal.open();
    this.reviewNominationViewModel.clearReject();
  }

  reviewRecognized(nomination) {
    this.selectedNomination = nomination;
    this.acknowledgmentService.findNominationsByAckId(nomination.ackId)
      .then((response) => {
        this.recognizedNominations = response;
      })
      .catch((error) => {
        this.logger.error('Details of error:', error);
        this.baseComponent.showMessageError();
      });
    this.reviewRecognizedModal.modal.open();
  }

  reviewRejected(nomination) {
    this.selectedNomination = nomination;
    this.reviewNominationModal.modal.open();
    this.reviewNominationViewModel.readRejectOnly(nomination.rejectMessage);
  }

  attached() {
    this.search();
  }

  exportToExcel(tableId) {
    const docName = 'export';
    return this.htmlToExcelExport.export(tableId, docName);
  }
}
