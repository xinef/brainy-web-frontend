import { inject, bindable } from 'aurelia-framework';
import { AcknowledgmentService } from '../services/acknowledgment-service';
import { BaseComponent } from '../../util/base-component';
import { constantsNominations } from '../../util/constant-nomination-messages';

@inject(AcknowledgmentService, BaseComponent)
export class ReviewRecognized {
  @bindable nomination = {};
  @bindable nominations = [];
  isLocked = false;
  constructor(acknowledgmentService, baseComponent) {
    this.acknowledgmentService = acknowledgmentService;
    this.baseComponent = baseComponent;
  }

  sendReview() {
  }

  get isAwarded() {
    return this.nomination.status === 'awarded';
  }

  sendEmail() {
    this.isLocked = true;
    this.acknowledgmentService.sendEmailAcknowledgmentAwarded(this.nomination.ackId)
       .then((response) => {
         this.processAwardedNominationResponse(response);
         this.closeRecognizedModal();
       })
       .catch((error) => {
         this.logger.error('Details of the error:', error);
         this.baseComponent.showMessageError();
       })
       .then(() => {
         this.baseComponent.dismissProgressHub();
       });
  }

  processAwardedNominationResponse(response) {
    const message = constantsNominations.NOMINATION_MESSAGECODE[response.messageCode];
    if (response.messageCode === 'email.sent') {
      this.baseComponent.showMessageSuccess(message);
      this.nomination.status = 'awarded';
    } else {
      this.baseComponent.showMessageError(message);
    }
  }

  closeRecognizedModal() {
    this.isLocked = false;
  }
}
