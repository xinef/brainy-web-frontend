// eslint-disable-next-line import/no-extraneous-dependencies
import dateFormat from 'dateformat';

import { inject, LogManager } from 'aurelia-framework';
import { AcknowledgmentClient } from '../../clients/acknowledgment-client';
import Nomination from '../../models/nomination';

@inject(AcknowledgmentClient)
export class AcknowledgmentService {

  constructor(acknowledgmentClient) {
    this.acknowledgmentClient = acknowledgmentClient;
    this.logger = LogManager.getLogger(AcknowledgmentService.name);
  }

  saveNomination(nomination) {
    return this.acknowledgmentClient.saveNomination(nomination);
  }

  getNominations(nominationFilter) {
    const querystring = this.getQueryStringFromNominationFilter(nominationFilter);
    return this.acknowledgmentClient.getNominations(querystring)
    .then((response) => {
      if (response.messageCode) {
        return Promise.reject(response);
      }
      return response.map(n => new Nomination(n));
    });
  }

  findNominationsByAckId(ackId) {
    return this.acknowledgmentClient.findNominationsByAckId(ackId)
    .then((response) => {
      if (response.messageCode) {
        return Promise.reject(response);
      }
      return response.map(n => new Nomination(n));
    });
  }

  getQueryStringFromNominationFilter(nominationFilter) {
    return $.param(this.getFormatedFilter(nominationFilter)).toLowerCase();
  }

  getFormatedFilter(nominationFilter) {
    const n = nominationFilter;
    const filter = {
      nominatorName: n.nominatorName,
      nomineeName: n.nomineeName,
      competence: n.competence,
      status: n.status,
      headquarter: n.headquarter,
      page: n.page,
      items: n.items,
    };

    if (n.startDate) {
      filter.startDate = dateFormat(n.startDate, 'yyyy-mm-dd');
    }

    if (n.endDate) {
      filter.endDate = dateFormat(n.endDate, 'yyyy-mm-dd');
    }

    return filter;
  }

  sendEmailAcknowledgmentAwarded(ackId) {
    return this.acknowledgmentClient.updateAcknowledgement(ackId);
  }

  acceptRejectNomination(idNomination, confirmation) {
    return this.acknowledgmentClient.acceptRejectNomination(idNomination, confirmation);
  }

}
