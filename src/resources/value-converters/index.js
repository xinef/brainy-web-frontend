export function configure(config) {
  config.globalResources([
    './number-format',
    './string-to-lowercase',
    './object-keys',
  ]);
}
