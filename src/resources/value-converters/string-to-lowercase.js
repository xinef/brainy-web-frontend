
export class ToLowercaseValueConverter {
  toView(value = '') {
    return value.toLowerCase();
  }
  fromView(value = '') {
    return value.toLowerCase();
  }
}
