import { inject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { Tag } from '../../models/tag';

@inject(EventAggregator)
export class CreateTagModal {

  constructor(eventAggregator) {
    this.eventAggregator = eventAggregator;
    this.tag = new Tag();
  }

  bind() {
    this.subscriptionForEditTagToInsert = this.eventAggregator
        .subscribe('edit-tag-to-insert', (tags) => {
          this.tags = tags;
          this.openModal();
        });
    this.subscriptionForTagCreated = this.eventAggregator
        .subscribe('tag-created', () => this.closeModal());
  }

  unbind() {
    this.subscriptionForEditTagToInsert.dispose();
    this.subscriptionForTagCreated.dispose();
  }

  get isValidTag() {
    return this.tag.isValid && !this.tag.isDuplicatedIn(this.tags);
  }

  openModal() {
    this.newTagDialog.open();
  }

  closeModal() {
    this.clearTag();
    this.newTagDialog.close();
  }

  clearTag() {
    this.tag = new Tag();
  }

  createTag() {
    this.eventAggregator.publish('tag-to-create', this.tag);
  }

}
