export function configure(config) {
  config.globalResources([
    './brainy-autocomplete-drop-down-list',
    './brainy-input',
    './create-tag-modal',
    './nominate-modal',
    './notification-modal',
    './tag-list',
    './rating',
    './brainy-rating.html',
  ]);
}
