import { inject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { BaseComponent } from '../../util/base-component';
import { Notification } from '../../models/notification';

@inject(EventAggregator, BaseComponent)
export class NotificationModal {

  constructor(eventAggregator, baseComponent) {
    this.eventAggregator = eventAggregator;
    this.baseComponent = baseComponent;
    this.notification = new Notification();
    this.skill = { name: '', level: '' };
  }

  bind() {
    this.subscriptionForNotificationModal = this.eventAggregator
        .subscribe('new-notification-modal', () => this.openNotificationModal());
  }

  unbind() {
    this.subscriptionForNotificationModal.dispose();
  }

  openNotificationModal() {
    this.newNotificationModal.open();
  }

  closeNotificationModal() {
    this.newNotificationModal.close();
  }

  addSkill() {
    if (this.skill) {
      this.notification.skills.push(this.skill);
      this.skill.name = '';
      this.skill.level = '';
    }
  }

  removeSkill(skill) {
    const index = this.notification.skills.indexOf(skill);
    if (index !== -1) {
      this.notification.skills.splice(index, 1);
    }
  }

  createNotification() {

  }

}
