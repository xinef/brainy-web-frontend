import { inject, LogManager } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { ProfileService } from '../../profile/services/profile-service';
import { AcknowledgmentService } from '../../acknowledgment/services/acknowledgment-service';
import { BaseComponent } from '../../util/base-component';
import { constantsNominations } from '../../util/constant-nomination-messages';
import localStorageManager from '../../util/local-storage-manager';
import Nomination from '../../models/nomination';
import { constants } from '../../util/constants';

@inject(EventAggregator, ProfileService, AcknowledgmentService, BaseComponent)
export class NominateModal {

  checked = false;
  isAdmin = false;
  clientName= '';

  constructor(eventAggregator, profileService, acknowledgmentService, baseComponent) {
    this.eventAggregator = eventAggregator;
    this.profileService = profileService;
    this.acknowledgmentService = acknowledgmentService;
    this.baseComponent = baseComponent;
    this.nomination = new Nomination();
    this.logger = LogManager.getLogger(AcknowledgmentService.name);
  }

  bind() {
    this.subscriptionForNominateModal = this.eventAggregator.subscribe('nominate-modal', (user) => {
      this.nomination.nomineeName = user.names;
      this.nomination.nomineeEmail = user.email;
      this.nomination.headquarter = user.headquarter;
      this.isAdmin = localStorageManager.isAdmin();
      this.openNominateModal();
    });
  }

  unbind() {
    this.subscriptionForNominateModal.dispose();
  }

  get isValidNomination() {
    const nom = this.nomination;
    const valid = nom.nomineeName && nom.competence && nom.description;
    if (this.checked) {
      return (constants.NAME_REGEX.test(this.clientName) && valid);
    }
    return valid;
  }

  openNominateModal() {
    this.nominateModal.open();
  }

  closeNominateModal() {
    this.nominateModal.close();
    this.restartValues();
  }

  processNomination() {
    const jsonBody = localStorageManager.getJsonWebTokenBody();

    this.getUserNominatorInformation(jsonBody.unique_name).then((user) => {
      this.baseComponent.showProgressHub();
      this.setNominator(user);

      this.acknowledgmentService.saveNomination(this.nomination)
      .then((response) => {
        this.processSaveNominationResponse(response);
      })
      .catch((error) => {
        this.logger.error('Details of the error:', error);
        this.baseComponent.showMessageError();
      })
      .then(() => {
        this.baseComponent.dismissProgressHub();
      });
    });
  }

  getUserNominatorInformation(username) {
    return this.profileService.getProfileMinimalInformation(username);
  }

  setNominator(user) {
    if (this.checked) {
      this.nomination.nominatorName = this.clientName;
      this.nomination.nominatorPosition = 'Client';
    } else {
      this.nomination.nominatorName = user.names;
      this.nomination.nominatorPosition = user.position;
    }
    this.nomination.nominatorEmail = user.email;
  }

  restartValues() {
    this.nomination = new Nomination();
    this.checked = false;
    this.clientName = '';
  }

  getMessageFromMessageCode(messageCode) {
    return constantsNominations.NOMINATION_MESSAGECODE[messageCode];
  }

  processSaveNominationResponse(response) {
    const message = this.getMessageFromMessageCode(response.messageCode);
    if (response.messageCode === 'success') {
      this.baseComponent.showMessageSuccess(message);
      this.closeNominateModal();
    } else {
      this.baseComponent.showMessageError(message);
    }
  }
}
