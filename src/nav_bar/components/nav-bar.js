import { EventAggregator } from 'aurelia-event-aggregator';
import { inject, bindable } from 'aurelia-framework';
import { I18N } from 'aurelia-i18n';
import { BaseComponent } from '../../util/base-component';
import { constants } from '../../util/constants';
import localStorageManager from '../../util/local-storage-manager';

@inject(I18N, EventAggregator)
export class NavBar extends BaseComponent {
  @bindable router = null;
  isFirstLogin = false;
  isAdmin = false;
  singleHeadquarter = false;
  notificationAllowed = constants.NOTIFICATION_ALLOWED;

  constructor(i18n, eventAggregator) {
    super();
    this.i18n = i18n;
    this.eventAggregator = eventAggregator;
    this.headquarters = [];
  }

  bind() {
    const that = this;
    that.isAdmin = localStorageManager.isAdmin();
    this.subscriptionGetHeadquarters = this.eventAggregator
    .subscribe('application-parameter-initialized',
    (result) => {
      that.isAdmin = result.isAdmin;
      that.headquarters = result.headquarters;
      that.singleHeadquarter = that.headquarters !== undefined && that.headquarters.length === 1;
      that.selectedHeadquarter = that.headquarters !== undefined ?
                                 that.headquarters[0] : '';
    });
  }

  unbind() {
    this.subscriptionGetHeadquarters.dispose();
  }

  attached() {
    this.subscriber = this.ea.subscribe(constants.UPDATE_NAV_BAR_WHEN_USER_LOGGED, () => {
      this.updateNavBarWhenUserLogged();
    });
  }

  detached() {
    this.subscriber.dispose();
  }

  updateNavBarWhenUserLogged() {
    this.isFirstLogin = localStorageManager.isFirstLogin();
  }

  changeLanguage(language) {
    this.i18n.setLocale(language);
  }

  changeHeadquarter(headquarter) {
    this.selectedHeadquarter = headquarter;
    this.eventAggregator.publish('nav-bar-change-headquarter',
      { selectedHeadquarter: this.selectedHeadquarter });
  }

  logout() {
    this.isFirstLogin = false;
    this.isAdmin = false;
    this.singleHeadquarter = false;
    localStorageManager.removeAuthToken();
    localStorageManager.setFirstLogin(this.isFirstLogin);
    localStorageManager.setIsAdmin(this.isAdmin);
    localStorageManager.setHasDashboard(false);
    this.navigation.navigateToLogin();
  }

  get isAuthenticated() {
    return this.auth.isAuthenticated();
  }

}
