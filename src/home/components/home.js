import { inject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { HomeService } from '../services/home-service';
import { constants } from '../../util/constants';
import constantsMessage from '../../util/constants-message';
import { BaseComponent } from '../../util/base-component';
import ApplicationParameter from '../../shared/application-parameter';

@inject(HomeService, BaseComponent, ApplicationParameter, EventAggregator)
export class Home {

  hasResults = false;
  notificationAllowed = constants.NOTIFICATION_ALLOWED;
  skill = {};

  constructor(homeService, baseComponent, applicationParameter, eventAggregator) {
    this.homeService = homeService;
    this.homeService.setViewModel(this);
    this.baseComponent = baseComponent;
    this.lastIndex = constants.DEFAULT_LAST_INDEX;
    this.lastOffset = constants.DEFAULT_LAST_OFFSET;
    this.applicationParameter = applicationParameter;
    this.eventAggregator = eventAggregator;
    this.applicationParameter.getParameters();
    this.loading = false;
  }

  setResults(results, total) {
    this.results = results;
    this.hasResults = total > 0;
    this.lastOffset = this.hasResults ? (this.lastOffset +
      constants.ITEMS_PAGINATION) : this.lastOffset;
    this.totalSearch = total;
    this.stillMoreRows = this.results.length < total;
  }

  addResults(results) {
    this.results = this.results.concat(results);
    this.lastOffset = this.hasResults ? (this.lastOffset +
      constants.ITEMS_PAGINATION) : this.lastIndex;
    this.stillMoreRows = this.results.length < this.totalSearch;
    this.loading = false;
  }

  doSearch(query) {
    this.lastQuery = query;
    this.lastOffset = constants.DEFAULT_LAST_OFFSET;
    let aSearch = null;
    let text = query;
    if (typeof (query) === 'object') {
      aSearch = query;
      text = aSearch.choice === 'name' ? aSearch.name : aSearch.skill;
    }
    if (text !== '' && (!constants.NAME_REGEX.test(text) || !constants.SKILL_REGEX.test(text))) {
      this.baseComponent.showMessageError(constantsMessage.QUERY_INVALID_MESSAGE);
    } else if (aSearch != null) {
      this.homeService.advancedSearchFromAPI(aSearch.name, aSearch.skill, aSearch.headquarter,
                      aSearch.location, aSearch.englishLevel, aSearch.position,
                      constants.ITEMS_PAGINATION, constants.DEFAULT_LAST_OFFSET);
    } else {
      this.homeService.searchFromAPI(query,
                constants.ITEMS_PAGINATION, constants.DEFAULT_LAST_OFFSET);
    }
  }


  moreResults() {
    this.loading = true;
    const aSearch = this.lastQuery;
    if (typeof (aSearch) === 'object') {
      this.homeService.advancedSearchFromAPI(aSearch.name, aSearch.skill, aSearch.headquarter,
                      aSearch.location, aSearch.englishLevel, aSearch.position,
                      constants.ITEMS_PAGINATION, this.lastOffset);
    } else {
      this.homeService.searchFromAPI(aSearch, constants.ITEMS_PAGINATION, this.lastOffset);
    }
  }

  openNotificationModal() {
    this.eventAggregator.publish('new-notification-modal');
  }
}
