import { inject, bindable } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import ApplicationParameter from '../../shared/application-parameter';
import { BaseComponent } from '../../util/base-component';
import ManagementService from '../../management/services/management-service';
import { HomeService } from '../services/home-service';

@inject(EventAggregator, ApplicationParameter, BaseComponent, ManagementService, HomeService)
export class Search {
  @bindable execute;
  safeQuery = '';
  checked = false;

  constructor(eventAggregator, applicationParameter, baseComponent, managementService,
    homeService) {
    this.eventAggregator = eventAggregator;
    this.applicationParameter = applicationParameter;
    this.baseComponent = baseComponent;
    this.managementService = managementService;
    this.homeService = homeService;
    this.headquarters = [];
    this.englishLevels = [];
    this.locations = [];
    this.positions = [];
    this.advancedSearch = { choice: 'name', name: '', skill: '', englishLevel: '', headquarter: '', location: '', position: '' };
  }

  created() {
    this.homeService.getEnglishLevels().then((englishLevels) => {
      this.englishLevels = englishLevels.elements;
    });
    this.homeService.getPositions().then((positions) => {
      this.positions = positions.elements;
    });
    this.managementService.getHeadquarters().then((headquarters) => {
      this.headquarters = headquarters;
    });
  }

  headquarterChanged(event) {
    this.getParametersByHeadquarter(event.srcElement.value);
  }

  getParametersByHeadquarter(headquarter) {
    this.managementService.getParameters(headquarter).then((parameters) => {
      const locations = [];
      Object.keys(parameters.parameterProperty.location).forEach((key) => {
        if (Object.prototype.hasOwnProperty.call(parameters.parameterProperty.location, key)) {
          locations.push(key);
        }
      });
      this.locations = locations;
    })
    .then(() => {
      this.advancedSearchViewModel.selectLocations.refresh();
    });
  }

  executeSearch() {
    if (this.checked) {
      if (this.advancedSearch.choice === 'name') {
        this.advancedSearch.name = this.query;
        this.advancedSearch.skill = '';
      } else {
        this.advancedSearch.skill = this.query;
        this.advancedSearch.name = '';
      }

      const advanced = Object.assign({}, this.advancedSearch);
      advanced.englishLevel = encodeURIComponent(advanced.englishLevel.replace('+', '\\+'));
      this.execute({ query: advanced });
    } else {
      this.execute({ query: this.query });
    }
  }

  get query() {
    return this.safeQuery;
  }

  set query(newValue) {
    this.safeQuery = newValue;
  }
}
