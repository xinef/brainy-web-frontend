import { inject, LogManager } from 'aurelia-framework';
import { ProfileClient } from '../../clients/profile-client';
import { BaseComponent } from '../../util/base-component';

@inject(BaseComponent, ProfileClient)
export class UserDetailService {

  constructor(baseComponent, profileClient) {
    this.baseComponent = baseComponent;
    this.profileClient = profileClient;
    this.logger = LogManager.getLogger(UserDetailService.name);
  }

  setViewModel(viewModel) {
    this.viewModel = viewModel;
  }

  getMostEndorsedSkills(email) {
    return this.profileClient.retrieveMostEndorsedSkills(email)
        .then(skills => skills)
        .catch((error) => {
          this.logger.error('Details of the error:', error);
          this.baseComponent.showMessageError();
        });
  }

  navigateToProfile(user) {
    if (user.isCurrentUser()) {
      this.baseComponent.navigation.navigateToProfile();
    } else {
      this.baseComponent.navigation.navigateToProfileOf(user.email);
    }
  }
}
