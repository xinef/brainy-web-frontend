import { inject, LogManager } from 'aurelia-framework';
import { ProfileClient } from '../../clients/profile-client';
import { BaseComponent } from '../../util/base-component';
import localStorageManager from '../../util/local-storage-manager';
import { constants } from '../../util/constants';

@inject(BaseComponent, ProfileClient)
export class HomeService {

  constructor(baseComponent, profileClient) {
    this.baseComponent = baseComponent;
    this.profileClient = profileClient;
    this.logger = LogManager.getLogger(HomeService.name);
  }

  setViewModel(value) {
    this.viewModel = value;
  }

  searchFromAPI(query, items, offset) {
    if (offset === constants.DEFAULT_LAST_OFFSET) {
      this.baseComponent.showProgressHub();
    }
    return this.profileClient.retrieveListOfProfiles(query, items, offset)
      .then(users => this.processSearchResult(users, offset))
      .catch((error) => {
        this.logger.error('Details of the error', error);
        this.baseComponent.showMessageError();
      })
      .then(() => {
        if (offset === constants.DEFAULT_LAST_OFFSET) {
          this.baseComponent.dismissProgressHub();
        }
      });
  }

  advancedSearchFromAPI(name, skills, headquarter, locations, language, position, items, offset) {
    this.baseComponent.showProgressHub();

    return this.profileClient.retrieveListOfProfilesAdvanced(name, skills,
       headquarter, locations, language, position, items, offset)
      .then(users => this.processSearchResult(users, offset))
      .catch((error) => {
        this.logger.error('Details of the error', error);
        this.baseComponent.showMessageError();
      })
      .then(() => {
        this.baseComponent.dismissProgressHub();
      });
  }

  markCurrentUser(users) {
    const currentUser = users.find(user => (
      localStorageManager.isCurrentUser(user.email)
    ));

    if (currentUser) {
      currentUser.setCurrentUser(true);
    }
  }

  getPositions() {
    return this.profileClient.retrieveListOfPositions()
    .then(positions => positions);
  }

  getEnglishLevels() {
    return this.profileClient.retrieveListOfEnglishLevels()
    .then(englishLevels => englishLevels);
  }

  processSearchResult(users, offset) {
    let total = users.length;
    if (total > 0) {
      if (offset === constants.DEFAULT_LAST_OFFSET) {
        total = users[0].id;
        users.shift();
        const message = `There were ${total} people that match your search`;
        this.baseComponent.showMessageSuccess(message);
      }
      this.markCurrentUser(users);
    }
    if (offset === 0) {
      this.viewModel.setResults(users, total);
    } else {
      this.viewModel.addResults(users);
    }
  }
}
