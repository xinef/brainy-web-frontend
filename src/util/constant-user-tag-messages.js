export const constantsTagUserMessages = {
  TAG_USER_MESSAGECODE: {
    'tag.user.already.tagged': 'User is already tagged',
    'tag.user.cannot.tag.yourself': 'Cannot tag yourself',
    'tag.user.success': 'Your buddy was tagged',
  },
};
