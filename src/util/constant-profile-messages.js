export const constants = {
  PROFILE_MESSAGECODE: {
    'profile.notValid': 'You need to fill all the required fields',
    'profile.position.notValid': 'Position is not valid',
    'profile.headquarter.notValid': 'Headquarter is not valid',
    'profile.skillList.exceeded': 'You exceeded the limit of skills',
    'profile.skype.notValid': 'Skype Id is not valid',
    'profile.email.notValid': 'Email is not valid',
    'profile.summary.notValid': 'Summary is not valid',
    'profile.no.dashboard': 'Without dashboard',
    'profile.no.longer.available': 'The user you are looking for is no longer available' },
};
