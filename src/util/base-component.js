// eslint-disable-next-line
import { Container } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { AuthService } from 'aurelia-auth';
import { MdToastService } from 'aurelia-materialize-bridge';
import { Navigation } from './navigation';

export class BaseComponent {

  constructor() {
    this.ea = Container.instance.get(EventAggregator);
    this.auth = Container.instance.get(AuthService);
    this.navigation = Container.instance.get(Navigation);
    this.toast = Container.instance.get(MdToastService);
  }

  showProgressHub() {
    this.notify('showProgressHub');
  }

  dismissProgressHub() {
    this.notify('dismissProgressHub');
  }

  showMessageError(errorMessage = 'An error occurred. Please try again') {
    this.toast.show(errorMessage, 6000);
  }

  showMessageSuccess(successMessage = 'Everything worked fine!') {
    this.toast.show(successMessage, 5000);
  }

  notify(key) {
    this.ea.publish(key);
  }
}
