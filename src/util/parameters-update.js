import { inject } from 'aurelia-framework';
import ManagementService from '../management/services/management-service';
import Parameter from '../models/parameter';

@inject(ManagementService)
export default class ParametersUpdate {

  constructor(managementService) {
    this.managementService = managementService;
  }

  getParamsAndHqs(user, isAdmin) {
    let p1;
    if (isAdmin) {
      p1 = this.managementService.getParametersByAdministrator(user.email);
    } else if (user.headquarter == null) {
      p1 = new Parameter();
    } else {
      p1 = this.managementService.getParameters(user.headquarter);
    }
    const p2 = this.managementService.getHeadquarters();
    return Promise.all([p1, p2]);
  }
}
