/* global window */

export const constants = {
  baseUrl: window.env.BASE_SERVER_URL,
  microserviceSkillUrl: window.env.MICROSERVICE_SKILL_URL,
  outlookBaseUrl: window.env.OUTLOOK_BASE_URL,
  urlRedirect: window.env.URL_REDIRECT,
  gravatarUrl: 'https://www.gravatar.com/avatar/',
  clientIdOutLook: window.env.OUTLOOK_ID,
  scopeGraphOutlook: 'https://graph.microsoft.com/user.read',
  AURELIA_TOKEN: 'aurelia_token',
  ACCESS_TOKEN: 'access_token',
  FIRST_LOGIN: 'first_login',
  ISADMIN: 'isAdmin',
  HAS_DASHBOARD: 'hasDashboard',
  BAD_REQUEST: 400,
  EXPIRED_TOKEN_STATUS: 401,
  FIRST_LOGIN_STATUS: 404,
  SUCCESS_STATUS: 200,
  NO_CONTENT: 204,
  INTERNAL_SERVER_ERROR: 500,
  UPDATE_NAV_BAR_WHEN_USER_LOGGED: 'updateNavBarWhenUserLogged',
  SKYPE_REGEX: /^([a-z]+:)?([a-z][a-z0-9-_.]{5,31})$/,
  SKILL_REGEX: /^[a-z][\da-z]+(\s[\da-z]+)*$/,
  TAG_REGEX: /^[a-z\d]+(\s[a-z\d]+)*$/,
  NAME_REGEX: /^[a-z\d]+(\s[a-z\d]+)*$/,
  EMPTY_STRING: '',
  NOTIFICATION_ALLOWED: false,
  keyCodes: {
    ENTER: 13,
    ESCAPE: 27,
    END: 35,
    HOME: 36,
    LEFT_ARROW: 37,
    UP_ARROW: 38,
    RIGHT_ARROW: 39,
    DOWN_ARROW: 40,
    SHIFT: 16,
  },
  COMPETENCIES: ['Passion', 'Commitment', 'Innovation', 'Quality', 'Teamwork'],
  NOMINATION_STATUSES: ['pending', 'rejected', 'accepted', 'acknowledged', 'awarded'],
  DEFAULT_LAST_INDEX: 0,
  DEFAULT_LAST_OFFSET: 0,
  DEFAULT_LAST_PAGE: 0,
  ITEMS_PAGINATION: 10,
  DEFAULT_RADIX: 10,
};
