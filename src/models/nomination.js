export default class Nomination {

  constructor(attributes = {}) {
    this.id = attributes.id || null;
    this.nomineeName = attributes.nomineeName || '';
    this.nomineeEmail = attributes.nomineeEmail || '';
    this.nominatorName = attributes.nominatorName || '';
    this.nominatorEmail = attributes.nominatorEmail || '';
    this.nominatorPosition = attributes.nominatorPosition || '';
    this.description = attributes.description || '';
    this.competence = attributes.competence || '';
    this.status = attributes.status || '';
    this.ackId = attributes.ackId || null;
    this.dateNomination = attributes.dateNomination || '';
    this.dateRejected = attributes.dateRejected || '';
    this.dateAccepted = attributes.dateAccepted || '';
    this.dateAcknowledged = attributes.dateAcknowledged || '';
    this.dateAwarded = attributes.dateAwarded || '';
    this.headquarter = attributes.headquarter || '';
    this.rejectMessage = attributes.rejectMessage || '';

    this.date = this.getDate(attributes);
  }

  getDate(attributes) {
    const map = { pending: 'formatedDateNomination', rejected: 'formatedDateRejected', accepted: 'formatedDateAccepted', acknowledged: 'formatedDateAcknowledged', awarded: 'formatedDateAwarded' };
    return attributes[map[attributes.status]];
  }

  get showButton() {
    return this.status !== 'accepted' && this.status !== 'rejected';
  }

  get showRejectMessage() {
    return this.status === 'rejected';
  }
}
