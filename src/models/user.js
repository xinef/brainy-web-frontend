import md5 from 'md5';
import { constants } from '../util/constants';
import { constants as constantsProfile } from '../util/constant-profile-messages';
import { Skill } from './skill';

const SMALL_IMAGE_SIZE = 200;
const BIG_IMAGE_SIZE = 400;

export class User {

  constructor(attributes) {
    this.id = attributes.id || '';
    this.names = attributes.names;
    this.email = attributes.email || '';
    this.summary = attributes.summary;
    this.position = attributes.position || '';
    this.skillList = this.buildSkills(attributes.skillList || []);
    this.skype = attributes.skype;
    this.externalPhoto = `${constants.gravatarUrl + md5(this.email)}?default=identicon&size=`;
    this.englishLevel = attributes.englishLevel || '';
    this.location = attributes.location || '';
    this.headquarter = attributes.headquarter || '';
    this.entryDate = attributes.entryDate || '';
    this.exitDate = attributes.exitDate || '';
  }

  buildSkills(skills = []) {
    return skills.map(skill => new Skill(skill));
  }

  get getPosition() {
    return this.position === '-' ? constantsProfile.PROFILE_MESSAGECODE['profile.no.dashboard'] : this.position;
  }

  get photo() {
    return `${this.externalPhoto}${BIG_IMAGE_SIZE}`;
  }

  get smallPhoto() {
    return `${this.externalPhoto}${SMALL_IMAGE_SIZE}`;
  }

  isCurrentUser() {
    return this.currentUser;
  }

  get isInactive() {
    return this.exitDate !== '';
  }

  get hasLanguage() {
    return this.englishLevel !== '' && this.englishLevel !== '-';
  }

  get hasNoPosition() {
    return this.position === '' || this.position === '-';
  }


  setCurrentUser(currentUser) {
    this.currentUser = currentUser;
  }

  setNames(names) {
    this.names = names;
  }

  showSkype() {
    // eslint-disable-next-line no-undef
    window.location = `skype:${this.skype}?chat`;
  }

}
