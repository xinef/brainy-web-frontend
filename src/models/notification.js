export class Notification {

  constructor(attributes = {}) {
    this.id = attributes.id || null;
    this.skills = attributes.skills || [];
    this.ownerId = attributes.ownerId || '';
  }
}
