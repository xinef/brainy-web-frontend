import { EventAggregator } from 'aurelia-event-aggregator';
import { inject } from 'aurelia-framework';
import { ProfileService } from '../profile/services/profile-service';
import ManagementService from '../management/services/management-service';
import LocalStorageManager from '../util/local-storage-manager';
import Parameter from '../models/parameter';
import ParametersUpdate from '../util/parameters-update';

@inject(ProfileService, ManagementService, LocalStorageManager, ParametersUpdate, EventAggregator)
export default class ApplicationParameter {
  constructor(profileService, managementService, storageManager, parametersUpdate,
    eventAggregator) {
    this.profileService = profileService;
    this.eventAggregator = eventAggregator;
    this.managementService = managementService;
    this.storageManager = storageManager;
    this.parametersUpdate = parametersUpdate;
    const methods = { competences: () => this._getList('competence'), locations: () => this._getList('location') };
    this.model = Object.assign({ parameters: new Parameter(), headquarters: [] }, methods);
    this.userHqs = []; // list of headquarters to populate in dropdown(admin) or show for a user
    this.currentHeadquarter = '';
    this.parametersAllowed = {}; // parameters an admin can use
    this.managementService.getAdministrators().then((emailAdmins) => {
      this.emailAdmins = emailAdmins;
      this.isAdmin = this.computeIsAdmin();
      this.eventAggregator.publish('is-admin-has-value', { isAdmin: this.isAdmin });
    });
    this.subscribeEvents();
  }

  subscribeEvents() {
    const that = this;
    this.subscriptionChangeHeadquarter = this.eventAggregator
    .subscribe('nav-bar-change-headquarter',
    (result) => {
      that.currentHeadquarter = result.selectedHeadquarter.code;
      this.eventAggregator.publish('application-parameter-change-headquarter',
        { model: that.parametersAllowed[that.currentHeadquarter] });
    });
  }

  unbind() {
    this.subscriptionChangeHeadquarter.dispose();
  }

  processValues(values) {
    const [parameters, headquarters] = values;
    if (this.isAdmin) {
      this._updateAdminParameters(parameters, headquarters);
    } else {
      this._updateUserParameters(parameters, headquarters);
    }
    this.model.headquarters = headquarters;
    this._notifyParameterInitialized();
  }

  getCurrentParameters() {
    return this.parametersAllowed[this.currentHeadquarter];
  }

  _updateUserParameters(parameters, headquarters) {
    this.model.parameters = parameters;
    this.userHqs = headquarters.filter(h => h.code === parameters.code);
  }

  _updateAdminParameters(parameters, headquarters) {
    this.parametersAllowed = parameters;
    const keys = Object.keys(parameters);
    this.userHqs = headquarters.filter(h => keys.indexOf(h.code) !== -1);
    this.model.parameters = parameters[this.currentHeadquarter];
  }


  setCurrentHeadquarter(headquarter) {
    this.currentHeadquarter = headquarter;
  }

  getParameters() {
    return new Promise((resolve) => {
      if (!this.model.parameters.code) {
        this._updateFromService(resolve);
      } else {
        resolve(this.model);
      }
    });
  }

  _notifyParameterInitialized() {
    if (this.isAdmin) {
      this._sendNotification();
    } else {
      this.eventAggregator.subscribe('is-admin-has-value',
      (result) => {
        this.isAdmin = result.isAdmin;
        this._sendNotification();
      });
    }
  }

  _sendNotification() {
    this.eventAggregator.publish('application-parameter-initialized',
      {
        isAdmin: this.isAdmin,
        headquarters: this.userHqs,
      });
  }
  _getList(name) {
    if (this.model.parameters.parameterProperty) {
      return this.model.parameters.parameterProperty[name];
    }
    return null;
  }

  _updateFromService(resolve) {
    const jsonBody = this.storageManager.getJsonWebTokenBody();
    const email = jsonBody.unique_name;
    const that = this;
    this.profileService.getProfileInformation(email)
    .then((user) => {
      // const isAdmin = this.computeIsAdmin();
      that.currentHeadquarter = user.headquarter;
      return this.parametersUpdate.getParamsAndHqs(user, this.isAdmin);
    })
    .then((values) => {
      that.processValues(values);
      resolve(that.model);
    });
  }

  computeIsAdmin() {
    const emailUser = this.storageManager.getEmailUser();
    this.isAdmin = this.emailAdmins && this.emailAdmins.indexOf(emailUser) > -1;
    return this.isAdmin;
  }
}
