import { inject } from 'aurelia-framework';
// import { constants } from '../../util/constants';
import ManagementService from '../services/management-service';
import ApplicationParameter from '../../shared/application-parameter';

@inject(ManagementService, ApplicationParameter)
export class ConfigurationManager {

  constructor(managementService, applicationParameter) {
    this.managementService = managementService;
    this.applicationParameter = applicationParameter;
    this.parametersPromise = applicationParameter.getParameters();
    this.parametersPromise.then(this.setParameters.bind(this));
    this.competencies = {};
  }

  setParameters(model) {
    this.headquarter = model.parameters.code;
    this.competencies = model.competences();
  }

  search() {
    // this.parametersPromise.then(this.getNominations.bind(this));
  }

  attached() {
    // this.search();
  }
}
