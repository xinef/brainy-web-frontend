import { inject } from 'aurelia-framework';
import { Client } from './client';

@inject(Client)
export class AcknowledgmentClient {

  constructor(client) {
    this.client = client;
  }

  saveNomination(nomination) {
    return this.client.postTo('/acknowledgmentApi/nomination', nomination)
    .then(response => response.json());
  }

  getNominations(searchFilter) {
    return this.client.getFrom(`/acknowledgmentApi/nomination?${searchFilter}`)
    .then(response => response.json());
  }

  findNominationsByAckId(ackId) {
    return this.client.getFrom(`/acknowledgmentApi/nominations/${ackId}`)
    .then(response => response.json());
  }

  updateAcknowledgement(ackId) {
    return this.client.putTo(`/acknowledgmentApi/acknowledgment/${ackId}`)
    .then(response => response.json());
  }

  acceptRejectNomination(idNomination, confirmation) {
    return this.client.putTo(`/acknowledgmentApi/nomination/${idNomination}`, confirmation);
  }

  getAcknowledgments(nomineeEmail, headquarter) {
    return this.client.getFrom(`/acknowledgmentApi/acknowledgment?nomineeemail=${nomineeEmail}&headquarter=${headquarter}`)
    .then(response => response.json());
  }
}
