import { inject } from 'aurelia-framework';
import { User } from '../models/user';
import { Skill } from '../models/skill';
import { Client } from './client';

@inject(Client)
export class ProfileClient {

  constructor(client) {
    this.client = client;
  }

  getProfileInformation(username) {
    return this.requestRawProfileInformation(username)
      .then(response => response.json())
      .then(response => new User(response));
  }

  getProfileMinimalInformation(username) {
    return this.client.getFrom(`/profileApi/profile/user/${username}`)
      .then(response => response.json());
  }

  getDashboardInformation(email) {
    return this.client.getFrom(`/profileApi/profile/dashboard/${email}`)
      .then(response => response.json());
  }

  requestRawProfileInformation(username) {
    return this.client.getFrom(`/profileApi/profile/${username}`);
  }

  saveUserInformation(user) {
    return this.client.postTo('/profileApi/profile', user)
      .then(response => response.json());
  }

  retrieveListOfProfiles(query, items, offset) {
    return this.client.getFrom(`/profileApi/profiles?find=${query}&items=${items}&offset=${offset}`)
      .then(response => response.json())
      .then(response => response.elements.map(u => new User(u)));
  }

  retrieveListOfProfilesAdvanced(name, skills, headquarter, location, englishLevel,
    position, items, offset) {
    return this.client.getFrom(`/profileApi/profiles/advanced?name=${name}&skills=${skills}&headquarter=${headquarter}&location=${location}&language=${englishLevel}&position=${position}&items=${items}&offset=${offset}`)
      .then(response => response.json())
      .then(response => response.elements.map(u => new User(u)));
  }

  retrieveMostEndorsedSkills(email) {
    return this.client.getFrom(`/profileApi/profile/${email}/mostEndorsedSkills`)
      .then(response => response.json())
      .then(response => response.elements.map(s => new Skill(s)));
  }

  retrieveListOfPositions() {
    return this.client.getFrom('/profileApi/profiles/positions')
      .then(response => response.json());
  }

  retrieveListOfEnglishLevels() {
    return this.client.getFrom('/profileApi/profiles/english-levels')
      .then(response => response.json());
  }
}
