import { inject } from 'aurelia-framework';
import { User } from '../models/user';
import { Client } from './client';

@inject(Client)
export class UserTagClient {

  constructor(client) {
    this.client = client;
  }

  getMyTagsOnUser(email) {
    return this.client
        .getFrom(`/profileApi/profile/my-tags-on/${email}`)
        .then(response => response.json())
        .then(jsonResponse => jsonResponse.elements);
  }

  tagUser(email, idTag) {
    return this.client
        .postEmptyBodyTo(`/profileApi/profile/${email}/tag-in/${idTag}`)
        .then(response => response.json());
  }

  deleteTagOnUser(email, idTag) {
    return this.client
        .deleteFrom(`/profileApi/profile/${email}/tag-in/${idTag}`);
  }

  findUsersByTagName(tagName) {
    return this.client.getFrom(`/profileApi/my-tags/${tagName}/people`)
        .then(response => response.json())
        .then(jsonResponse => this.parseJsonToUser(jsonResponse));
  }

  parseJsonToUser(data) {
    return data.elements.map(user => new User(user.userProfile));
  }
}
