import { inject } from 'aurelia-framework';
import { Client } from './client';

@inject(Client)
export class ManagementClient {

  constructor(client) {
    this.client = client;
  }

  getParameters(headquarter) {
    return this.client.getFrom(`/managementApi/parameter/${headquarter}`)
    .then(response => response.json());
  }

  getHeadquarters() {
    return this.client.getFrom('/managementApi/parameter/headquarter')
    .then(response => response.json());
  }

  getAdministrators() {
    return this.client.getFrom('/managementApi/administrator')
    .then(response => response.json());
  }

  getParametersByAdministrator(adminEmail) {
    return this.client.getFrom(`/managementApi/administrator/${adminEmail}/parameter`)
    .then(response => response.json());
  }
}
